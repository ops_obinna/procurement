

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" type="image/png" href="{{ url('img/logo.png') }} " type="image/x-icon">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>PRINT INVOICE</title>
  </head>
  <body class="container">

    	<!-- <a href="{{ route('invoice',['download'=>'pdf']) }}">Download PDF</a> -->
    <br><br><br>

    <table class="table table-borderless" style="font-size:1.2em;">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>

    </tr>
  </thead>
  <tbody>
    <tr>
  @if($buyers)
      <td>
        <h3>INVOICE</h3>
        <p>Invoice NO:<br>{{$buyers->invoice_id}}</p>

        <p>Billed To:<br>{{$buyers->buyer_fname}}  {{$buyers->buyer_sname}}</p>
        <p>Phone:<br> {{$buyers->buyer_phone}}</p>
        <p>Billing Address:<br>{{$buyers->buyer_address}} </p>

      </td>

      <td style="width:200px;">

      </td>

      <td>
        <img src="{{ asset('img/logo.png') }}" style="width:150px;height:80px;margin-left:20em;" alt="company_logo" align="right;">
        <p style="margin-left:20em;;"><br>No. 2 Michika Street, Off Ahmadu Bello Way, Area 11, Abuja.
          <br>Tel:08038048174,09052439933 <br>Email:gaskiyabiomedicalnigltd@gmail.com
          <br> <br>Date: {{date('Y-m-d')}}        </p>
      </td>

    </tr>


  </tbody>
</table>


<br>
        <div class="table-responsive" style="font-size:1.2em;">
          <table id="add-row" class="display table table-striped table-hover" >
            <thead>
              <tr>
                <th>SN</th>
                <th>Description</th>
                <th>Unit Cost</th>
                <th>Quantity</th>
                <th style="text-align:right;">Amount</th>

              </tr>
            </thead>

            <tbody>
	             @foreach ($carts as $key => $cart)
               <tr>
                <td> {{ ++$key }}</td>
                <td>{{ $cart->product_name}}</td>
                <td>{{ $cart->product_price}}</td>
                <td>{{ $cart->product_qty }}</td>
                <td style="text-align:right;">{{ $cart->sub_total }}</td>

              </tr>
              <tr>

              @endforeach
            </tbody>

          </table>

          <p style="text-align:right;"><strong>TOTAL:</strong><strike>N</strike> {{$itemTotal}}</p>


        </div>

        <br><br><br>
        <table class="table table-borderless" style="font-size:1.2em;">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col"></th>


            </tr>
          </thead>
          <tbody>
            <tr>
              <td><hr style="border: 1px solid black" width="50%" align="left" ></td>
              <td><hr style="border: 1px solid black" width="50%" align="right" ></td>
            </tr>
            <tr>
              <td>For: Management</td>
              <td style="text-align:right;">Customer Sign</td>
            </tr>
          </tbody>
        </table>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    @else
      <p>You Have not Made Any Sales Today</p>
    @endif
  </body>
</html>
