@extends('home')
@section('main')

<div class="content">
	@include('dashboard.headingtwo')
	@include('dashboard.errorSuccess')

	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">LIST OF USERS</h4>

									</div>
								</div>
								<div class="card-body">

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th>SN</th>
													<th>Name</th>
													<th>Surname</th>
													<th>Email</th>
                          <th>Role</th>
													<th>Action</th>
                          <th>Change Role</th>

												</tr>
											</thead>

											<tbody>
												@php
											    $sn = 1;
											    @endphp
												@foreach ($users as $user)
											   <tr>
											   		<td> {{$sn}} </td>
													<td>{{$user->first_name}}</td>
													<td>{{$user->surname}}</td>
													<td>{{$user->email}}</td>
													<td>{{$user->role}}</td>
													<td>
														<div class="form-button-action">

															<a href="{{route('user.destroy',['id'=>$user->id])}}" >
															<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Delete User">
																<i class="fa fa-times"></i>
															</button>
															</a>
														</div>
													</td>
                          <td>
                            @if($user->role =='admin')
                              <a href="{{route('user.demote',['id'=>$user->id])}}" class="btn btn-danger">Remove Admin</a>
                            @else
                            <a href="{{route('user.makeadmin',['id'=>$user->id])}}" class="btn btn-success">Make Admin</a>

                            @endif
                          </td>

												</tr>
												@php
											    $sn++;
											    @endphp
												@endforeach
											</tbody>

										</table>
									</div>
								</div>
							</div>

							</div>


</div>
@endsection
