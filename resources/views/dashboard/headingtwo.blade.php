				<div class="panel-header bg-primary-gradient">
					<div class="page-inner py-5">
						<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
							<div>
								<h2 class="text-white pb-2 fw-bold">GASKIYA BIOMEDICALS NIG. LIMITED</h2>
								<h5 class="text-white op-7 mb-2">Management Information System	</h5>
							</div>
							<div class="ml-md-auto py-2 py-md-0">
								<a href="{{url('/cart_clear')}}" class="btn btn-secondary btn-round mr-2">Clear Cart</a>
								<a href="{{url('/cart')}}" class="btn btn-secondary btn-round mr-2">View Cart</a>
								<a href="{{url('/assign_saling_price')}}" class="btn btn-secondary btn-round mr-2">Check Selling Prices</a>
								<a href="{{url('/addMoreProduct')}}" class="btn btn-secondary btn-round">Add New Products</a>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
